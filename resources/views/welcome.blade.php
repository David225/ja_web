
@extends('layouts.layouts')

@section('content')

<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="first-slide" src="img/meuble3.jpg" alt="First slide">
      <div class="container">
        <div class="carousel-caption text-left">
          <h1>SALON </h1>
          <p>souhaitez-vous avoir un salon plein de style ? Nos confections qui vous aideront à vous inspirer !</p>
          <p><a class="btn btn-lg btn-primary" href="{{url('/catalogue')}}" role="button">Voir plus</a></p>
        </div>
      </div>
    </div>
    <div class="carousel-item">
      <img class="second-slide" src="img/meuble5.jpg" alt="Second slide">
      <div class="container">
        <div class="carousel-caption">
          <h1>SALLE A MANGER </h1>
          <p>Vous rêvé d'un meuble de salle de bain moderne pour pimper l'interieur de votre pièce d'eau, nous somme la </p>
          <p><a class="btn btn-lg btn-primary" href="{{url('/catalogue')}}" role="button">Voir plus</a></p>
        </div>
      </div>
    </div>
    <div class="carousel-item">
      <img class="third-slide" src="img/meuble1.jpg" alt="Third slide">
      <div class="container">
        <div class="carousel-caption text-right">
          <h1>CHAMBRE </h1>
          <p>Eh oui, finles formes classiques à mourir d'ennui ! Désormais, la Chambre se veut pratique et fonctionnel.</p>
          <p><a class="btn btn-lg btn-primary" href="{{url('/catalogue')}}" role="button">Voir plus</a></p>
        </div>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>


@endsection
