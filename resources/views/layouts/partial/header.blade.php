
<nav class="navbar navbar-expand-md navbar-light bg-dark">
  <a class="navbar-brand" href="{{url('/welcome')}}"><img src="img/ja.ico" class="img-responsive logo"> </a>
  <button type="button" class="navbar-toggler bg-light" data-toggle="collapse" data-target="#nav">
     <span class="navbar-toggler-icon"></span>
   </button>
  <div class="collapse navbar-collapse justify-content-center" id="nav">
  <ul class="navbar-nav">
    <li class="nav-item dropdown">
      <a class="nav-link text-light font-weight-bold text-uppercase px-3 "  data-toggle="dropdown"
       href="{{url('/contact')}}">Salon</a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="{{url('/catalogue')}}">Meubles TV</a>
        <a class="dropdown-item" href="{{url('/catalogue')}}">Tables basse</a>
        <a class="dropdown-item" href="{{url('/catalogue')}}">Fauteuil et Canapé</a>
        <a class="dropdown-item" href="{{url('/catalogue')}}">bibliothèque</a>
        <a class="dropdown-item" href="{{url('/catalogue')}}">Table d'appoint</a>
        <a class="dropdown-item" href="{{url('/catalogue')}}">Etagière</a>
        <a class="dropdown-item" href="{{url('/catalogue')}}">Bibliotheque</a>
        <a class="dropdown-item" href="{{url('/catalogue')}}">Consol</a>

      </div>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link text-light font-weight-bold text-uppercase px-3 "  data-toggle="dropdown"
       href="#">Salle à manger</a
      >
      <div class="dropdown-menu">
        <a class="dropdown-item" href="{{url('/catalogue')}}">Tables à manger</a>
        <a class="dropdown-item" href="{{url('/catalogue')}}">Chaises</a>
        <a class="dropdown-item" href="{{url('/catalogue')}}">Vaisselier</a>
        <a class="dropdown-item" href="{{url('/catalogue')}}">Buffet</a>
        <a class="dropdown-item"  href="{{url('/catalogue')}}">Miroir</a>
        <a class="dropdown-item" href="{{url('/catalogue')}}">Tapis</a>
      </div>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link text-light font-weight-bold text-uppercase px-3 "  data-toggle="dropdown"
       href="#">Chambre</a
      >
      <div class="dropdown-menu">
        <a class="dropdown-item" href="{{url('/catalogue')}}">Lit</a>
        <a class="dropdown-item" href="{{url('/catalogue')}}">Chevet</a>
        <a class="dropdown-item"  href="{{url('/catalogue')}}">Commode</a>
        <a class="dropdown-item" href="{{url('/catalogue')}}">Armoire</a>
        <a class="dropdown-item" href="{{url('/catalogue')}}">Dressing</a>
        <a class="dropdown-item" href="{{url('/catalogue')}}">Coiffeuse</a>
        <a class="dropdown-item" href="{{url('/catalogue')}}">Banquet</a>


      </div>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link text-light font-weight-bold text-uppercase px-3 "  data-toggle="dropdown"
       href="#">Accessoire</a
      >
      <div class="dropdown-menu">
        <a class="dropdown-item" href="{{url('/catalogue')}}">Tapis</a>
        <a class="dropdown-item" href="{{url('/catalogue')}}">Lampe</a>
        <a class="dropdown-item"  href="{{url('/catalogue')}}">Plante </a>
        <a class="dropdown-item" href="{{url('/catalogue')}}">Tableau</a>
        <a class="dropdown-item" href="{{url('/catalogue')}}">Coussin</a>
      </div>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link text-light font-weight-bold text-uppercase px-3 "
       href="{{url('/apropos')}}">A propos</a></li>

    <li class="nav-item dropdown">
      <a class="nav-link text-light font-weight-bold text-uppercase px-3 "
       href="{{url('/contact')}}">Contact</a>

    </li>
  </ul>
</div>
</nav>
