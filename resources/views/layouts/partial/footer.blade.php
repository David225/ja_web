<style>
.footer{
  background: #303740;
  color:white;

  .links{
    ul {list-style-type: none;}
    li a{
      color: white;
      transition: color .2s;
      &:hover{
        text-decoration:none;
        color:#c3cedb;
        }
    }
  }
  .about-company{
    i{font-size: 25px;}
    a{
      color:white;
      transition: color .2s;
      &:hover{color:#c3cedb}

    }
  }
  .location{
    i{font-size: 18px;}
  }
  .copyright p{border-top:1px solid rgba(255,255,255,.1);}
}

.un{
  margin-left: 55px;
}
.deux{
  margin-left: 25px;

}

.fa {
padding: 20px;
font-size: 30px;
width: 50px;
text-align: center;
text-decoration: none;
border-radius: 50%; }



</style>


<footer>
<div class="mt-5 pt-5 pb-5 footer">
<div class="container">
  <div class="row">
    <div class="col-lg-5 col-xs-12 about-company">
      <h2 class="un">JA</h2>
      <p class="deux">Suivez moi sur  </p>
      <a href="https://www.facebook.com/joele.agouassi" class="fa fa-facebook"></a>
        <a href="https://www.instagram.com/joele.agouassi/" class="fa fa-instagram"></a>

      </p>
    </div>
    <div class="col-lg-3 col-xs-12 links">
      <h4 class="mt-lg-0 mt-sm-3">Links</h4>
        <ul class="m-0 p-0">

          <li> <a href="{{url('/catalogue')}}">Salon</a></li>
          <li> <a href="{{url('/catalogue')}}">Séjour</a></li>
          <li> <a href="{{url('/catalogue')}}">Chambre</a></li>
          <li> <a href="{{url('/catalogue')}}">Salle de bain</a></li>
          <li> <a href="{{url('/apropos')}}">A propos</a></li>
          <li> <a href="{{url('/contact')}}">Contact</a></li>
        </ul>
    </div>
    <div class="col-lg-4 col-xs-12 location">
      <h4 class="mt-lg-0 mt-sm-4">Retrouvez nous</h4>
      <p>à Adjame 220 logement, <br>en face de Fraterniter Matin</p>
      <p class="mb-0"><i class="fa fa-phone mr-3"></i>(+225) 07 09 94 94 14</p>
      <p><i class="fa fa-envelope mr-3"></i>joeleatchui@gmail.com</p>


    </div>
  </div>
  <div class="row mt-5">
    <div class="col copyright">
      <p class=""><small class="text-white-50">© 2021. Made by Dsama.</small></p>
    </div>
  </div>
</div>
</div>
</footer>
